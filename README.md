# PiHole

Repo hosts a docker image containing pihole and cloudflared to use https for dns lookups

## Getting started

### Install docker desktop
[Windows](https://download.docker.com/win/stable/Docker%20for%20Windows%20Installer.exe)

[Mac](https://download.docker.com/mac/stable/Docker.dmg)

### Password 
Do this before setting up pihole for the first time!

Set a password for your pihole by updating the WEBPASSWORD line in the file "env"

If you  happened to have run it before setting the password remove the following files:

```
rm etc-pihole/setupVars.conf

rm etc-pihole/setupVars.conf.update.bak
```

### osxscripts
These are three scripts to make life easier for OSx

 * pihole - takes down the container and brings it back up + configures your dns settings
 * pi - configures your dns settings
 * unpi - sets your dns settings to the defaults provided by the router

### winscripts
These are three scripts to make life easier for windows
 
 * pihole.bat - takes down the container and brings it back up + configures your dns settings
 * pi.bat - configures your dns settings
 * unpi.bat - sets your dns settings to the defaults provided by the router
