netsh interface ipv4 set dnsservers name="Local Area Connection" source=dhcp
netsh interface ipv6 set dnsservers name="Local Area Connection" source=dhcp

netsh interface ipv4 set dnsservers name="Wireless Network Connection" source=dhcp
netsh interface ipv6 set dnsservers name="Wireless Network Connection" source=dhcp

