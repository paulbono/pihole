FROM pihole/pihole:4.3.1-4

RUN wget https://bin.equinox.io/c/VdrWdbjqyF/cloudflared-stable-linux-amd64.deb && \
    sudo apt-get install ./cloudflared-stable-linux-amd64.deb && \
    sudo useradd -r -M -s /usr/sbin/nologin -c "Cloudflared user" cloudflared && \
    sudo passwd -l cloudflared && \
    sudo chage -E 0 cloudflared && \
    sudo chage -l cloudflared && \
    sudo rm ./cloudflared-stable-linux-amd64.deb && \
    sudo chown -v cloudflared:cloudflared /usr/local/bin/cloudflared 

COPY 21-cloud.sh /etc/cont-init.d/

